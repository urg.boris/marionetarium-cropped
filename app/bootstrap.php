<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//!![Branik, ]['78.102.153.12']
$configurator->setDebugMode(true); // enable for your remote IP
//$configurator->setDebugMode(false); // enable for your remote IP
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

//$configurator->createRobotLoader()
//	->addDirectory(__DIR__)
//	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
