<?php

namespace App\Model;

use Nette;
use Nette\Security\Permission;

class AuthorizatorFactory
{

    use Nette\SmartObject;

    const GUEST = 'guest';
    const SUPERVISOR = 'supervisor';
    const ADMIN = 'admin';
    const SUPERADMIN = 'superadmin';

    const RESOURCE_BACKEND = 'backend';

    const OPERATION_ADMINEDIT = 'adminEdit';
    const OPERATION_VIEW = 'view';
    const OPERATION_EDIT = 'edit';
    const OPERATION_ADDDOCUMENTS = 'addDocuments';
    
    public static $roles;
    
    /**
     * @return Nette\Security\Permission
     */
    public static function create()
    {
        
        self::$roles = [
            self::SUPERADMIN => 'Superadmin',
            self::ADMIN => 'admin',
            self::SUPERVISOR => 'supervisor',
            self::GUEST => 'guest',
        ];
        
        $acl = new Permission;

        $acl->addRole(self::GUEST);
        $acl->addRole(self::SUPERVISOR);
        $acl->addRole(self::ADMIN);
        $acl->addRole(self::SUPERADMIN);

        $acl->addResource(self::RESOURCE_BACKEND); 
        
        $acl->allow(self::SUPERADMIN, self::RESOURCE_BACKEND, Permission::ALL);
        $acl->allow(self::ADMIN, self::RESOURCE_BACKEND, [self::OPERATION_ADDDOCUMENTS, self::OPERATION_EDIT, self::OPERATION_VIEW]);
        $acl->allow(self::SUPERVISOR, self::RESOURCE_BACKEND, [self::OPERATION_ADDDOCUMENTS, self::OPERATION_VIEW]);        
        $acl->deny(self::GUEST, self::RESOURCE_BACKEND);

        return $acl;
    }
}