<?php
declare(strict_types=1);

namespace App\Model;

use Nette;

class GuestKeyManager {

    use Nette\SmartObject;

    private $lang = "cs";

    /**
     * @var Nette\Database\Context
     */
    private $database;
    
    private $guestKey;

    public function __construct($guestKey, Nette\Database\Context $database) {
        $this->database = $database;
        $this->guestKey = $guestKey;
    }
    
    public function getKey() {
        return $this->guestKey;        
    }

 
}
