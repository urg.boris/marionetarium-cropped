<?php

namespace App\Model;

use Nette;
use Nette\Utils\FileSystem;

class GalereyaGeneratorManager {

    use Nette\SmartObject;

    private $wwwDir;
    
    /** @var \App\Model\MarionetteManager */
    protected $databaseManager;
    
    /** @var Nette\Database\Context */
    protected $database;

    public function __construct($wwwDir, \App\Model\MarionetteManager $databaseManager, Nette\Database\Context $database) {
        $this->wwwDir = $wwwDir;
        $this->databaseManager = $databaseManager;
        $this->database = $database;
    }
    
    public function getGalleryDataFileName($group_id=null, $carver_id=null) {
        $relativeFileName = DIRECTORY_SEPARATOR.$this->getCorespondingFileName($group_id, $carver_id);
        $absolutFileName = $this->wwwDir.$relativeFileName;
        if(file_exists($absolutFileName)) {
            return $relativeFileName;
        } else {
            $this->generateData($group_id, $carver_id);
            //was it generated?
            if(file_exists($absolutFileName)) {
                return $relativeFileName;
            } else {
                return null;
            }
        }
    }
    
    public function generateData($group_id=null, $carver_id=null) {
        if($group_id!=null) {
            $marionettes = $this->database
                    ->table("marionettes")->select("code")->where("group_id = ?", $group_id);

            $items = $this->database
                    ->table("marionette_images")->where("code", $marionettes)->where("type != ?", 1)->order('type ASC, id ASC');

        } elseif($carver_id!=null) {
            $marionettes = $this->database
                    ->table("marionettes")->select("code")->where("carver_id = ?", $carver_id);

            $items = $this->database
                    ->table("marionette_images")->where("code", $marionettes)->where("type != ?", 1)->order('type ASC, id ASC');
        } else {
            $items = $this->database
                    ->table("marionette_images")->where("type != ?", 1)->order('type ASC, id ASC');
        }
        if($items->count()==0) {
            //generate nothing
        } else {
            $this->saveData($items, $group_id, $carver_id);
        }
        
    }

    public function saveData($items, $group_id=null, $carver_id=null) {
           $output = "[";
            $itemsCount = count($items);
            $i = 0;
            $name = null;
            $category = null;
            if($group_id!=null) {
                $category = $this->databaseManager->getGalereyaCategoryName('group', $group_id);
            } elseif ($carver_id!=null) {
                $category = $this->databaseManager->getGalereyaCategoryName('carver', $carver_id);
            } else {
                $category = "Celá sbírka";
            }
            $pathString = null;
            foreach ($items as $item) {

                if($item->type == 2 || $item->type == 3) {
                    $name = $item->code. " ". $this->databaseManager->getLocaleByKey('marionette.'.$item->code);
                } else {
                    $name = $item->code;
                }
                $pathString = "/data/".$item->path;//addslashes ("/data/".$item->path);
                $output .= sprintf("{\"lowsrc\": \"%s\", \"fullsrc\": \"%s\", \"description\":\"%s\", \"category\":\"%s\"}", $pathString, $pathString, $category.": ".$name, $category);
                if ($i != $itemsCount - 1) {
                    $output .= ",";
                }
                $i++;
            }
            
            $output .= "]";
            $file = $this->wwwDir.DIRECTORY_SEPARATOR.$this->getCorespondingFileName($group_id, $carver_id);
            
            FileSystem::write($file, $output, 0755);
    }
    
    public function getCorespondingFileName($group_id=null, $carver_id=null) {
        $dir = "js".DIRECTORY_SEPARATOR."GalereyaData";
        if($group_id!=null) {
            $fileName = $dir.DIRECTORY_SEPARATOR."group".$group_id.".json";
        } elseif ($carver_id!=null) {
            $fileName = $dir.DIRECTORY_SEPARATOR."carver".$carver_id.".json";    
        } else {
            $fileName = $dir.DIRECTORY_SEPARATOR."all.json";
        }
        return $fileName;
    }
    
    public function getGaleryTitle($group_id=null, $carver_id=null) {
        if($group_id!=null) {
            $title = $this->databaseManager->getGalereyaCategoryName('group', $group_id);
        } elseif ($carver_id!=null) {
            $title = $this->databaseManager->getGalereyaCategoryName('carver', $carver_id);  
        } else {
            $title = "Celá sbírka";
        }
        return $title;
    }

}
