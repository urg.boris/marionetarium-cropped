<?php

namespace App\Model;

use Nette;

class MarionetteManager {

    use Nette\SmartObject;

    private $lang = "cs";

    /**
     * @var Nette\Database\Context
     */
    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function getAll(): array {
        $mationettes = $this->database
                ->table("marionettes");
        $forTemplate = [];
        foreach ($mationettes as $marionette) {
            $marionetteArray = $this->prepareMarionetteForTemplate($marionette);
            $forTemplate[] = $marionetteArray;
        }
        return $forTemplate;
    }

    public function getMarionettesFromGroup($id): array {
        $mationettes = $this->database
                        ->table("marionettes")->where("group_id = ?", $id);
        $forTemplate = [];
        foreach ($mationettes as $marionette) {
            $marionetteArray = $this->prepareMarionetteForTemplate($marionette);
            $forTemplate[] = $marionetteArray;
        }
        return $forTemplate;
    }

    public function getMarionettesFromCarver($id): array {
        $mationettes = $this->database
                        ->table("marionettes")->where("carver_id = ?", $id);
        $forTemplate = [];
        foreach ($mationettes as $marionette) {
            $marionetteArray = $this->prepareMarionetteForTemplate($marionette);
            $forTemplate[] = $marionetteArray;
        }
        return $forTemplate;
    }

    public function prepareMarionetteForTemplate($marionette, bool $extended = false): array {
        $marionetteArray = [];
        $marionetteArray["id"] = $marionette->id;
        $marionetteArray["code"] = $marionette->code;
        $marionetteArray['name'] = $this->getLocaleByKey("marionette." . $marionette->code);
        $marionetteArray["groupId"] = $marionette->group_id;
        $marionetteArray["group"] = $marionette->group->code;
        $marionetteArray["carverId"] = $marionette->carver_id;
        $marionetteArray["carver"] = $this->getLocaleByKey("carver." . $marionette->carver->code);
        $marionetteArray["size"] = $marionette->size;
        $marionetteArray["code"] = $marionette->code;

        if ($marionette->locale_age != "") {
            $marionetteArray["age"] = $this->getLocaleByKey($marionette->locale_age);
        } else {
            $marionetteArray["age"] = "";
        }

        if ($extended == true) {
            $marionetteArray["status"] = $marionette->status;
            $marionetteArray["note"] = $marionette->note;

            if ($marionette->locale_comment != "") {
                $marionetteArray["comment"] = $this->getLocaleByKey($marionette->locale_comment);
            } else {
                $marionetteArray["comment"] = "";
            }

            if ($marionette->isHead) {
                $marionetteArray["isHead"] = true;
            } else {
                $marionetteArray["isHead"] = false;
            }
            $marionetteArray["storagePlace"] = $marionette->storagePlace->name;
        }

        return $marionetteArray;
    }

    public function getLocaleByKey(string $key) {
        $locale = $this->database
                ->table("locales")
                ->select("value")
                ->where("name = ?", $this->lang . "." . $key)
                ->fetch();
        if (!$locale) {
            //for fallbacking to key
            //$value=$key; OR
            //$value = $key;
        } else {
            $value = $locale->value;
        }
        return $value;
    }
    
    public function getGalereyaCategoryName(string $sectionName, $id) {
        if($sectionName == "group") {
            $section = $this->database
                    ->table("groups")
                    ->where("id=?",$id)
                    ->fetch();
        } elseif($sectionName == "carver") {
            $section = $this->database
                    ->table("carvers")
                    ->where("id=?",$id)
                    ->fetch();
        } else {
            throw new \RuntimeException("!(\$section == \"carver\" || \$section == \"group\")");
        }
        $locale = $this->database
                ->table("locales")
                ->select("value")
                ->where("name = ?", $this->lang . "." . $sectionName.".".$section->code)
                ->fetch();
        if (!$locale) {
            //for fallbacking to key
            //$value=$key; OR
            //$value = $this->lang . "." . $section.".".$section->code;
        } else {
            $value = $section->code." - ".$locale->value;
        }
        return $value;
    }

}
