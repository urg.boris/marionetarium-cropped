<?php

namespace App\Modules\Admin\Presenters;

use Nette;

class HomepagePresenter extends BaseAdminPresenter {


	public function __construct() {
            //parent::__construct($database);
	}

	public function actionDefault() {
            $this->redirect("Marionette:list");
	}

}
