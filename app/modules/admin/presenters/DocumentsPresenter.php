<?php

namespace App\Modules\Admin\Presenters;

use Nette;

class DocumentsPresenter extends BaseAdminPresenter {
       
	public function __construct(Nette\Database\Context $database, \App\Model\MarionetteManager $databaseManager, \App\Model\GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

	public function actionDefault() {

	}
        

}
