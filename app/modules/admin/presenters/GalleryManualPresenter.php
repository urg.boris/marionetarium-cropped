<?php

namespace App\Modules\Admin\Presenters;

use Nette;
use App\Model\MarionetteManager;


class GalleryManualPresenter extends BaseAdminPresenter {

	public function __construct(Nette\Database\Context $database, \App\Model\MarionetteManager $databaseManager, \App\Model\GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

	public function actionDefault() {
            $this->redirect(":groups");
	}
        
        public function actionGroups() {

        }
        
        public function actionCarvers() {

        }
        
        
}
