<?php

namespace App\Modules\Admin\Presenters;

use Nette;


class GroupPresenter extends BaseAdminPresenter {

	public function __construct(\App\Model\MarionetteManager $databaseManager, Nette\Database\Context $database, \App\Model\GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

	public function actionDefault() {

	}
        
        public function actionList() {
            $items = $this->database
                    ->table("groups")->where("main_section_id = ?", 1); //->limit(100);
            $this->template->items = [];
            foreach ($items as $item)
            {
                $this->template->items[] = $this->prepareForTemplate($item);
            }
        }

        private function prepareForTemplate($item): array {
            $forTemplate = [];
            $forTemplate["id"] = $item->id;
            $forTemplate["code"] = $item->code;
            $forTemplate['name'] = $this->databaseManager->getLocaleByKey($item->locale_name);
            $forTemplate["carverId"] = $item->carver_id;
            $forTemplate["carver"] = $this->databaseManager->getLocaleByKey("carver.".$item->carver->code);
            $forTemplate["age"] = $this->databaseManager->getLocaleByKey($item->locale_age);

            $forTemplate['count'] = (int) $this->database
                ->table("marionettes")
                ->where("group_id = ?", $item->id)
                ->count("id");
            $forTemplate['isFotoCollection'] =  $item->isFotoCollection;
            if( $item->isFotoCollection) {
                $forTemplate['cca'] = (int) $this->database
                        ->table("foto_collections")
                        ->where("group_id = ?", $item->id)
                        ->sum("orientationalMarionetteCount");
            } else {
                $forTemplate['cca'] = 0;
            }
            $forTemplate['totalCount'] = $forTemplate['count']+$forTemplate['cca'];
            return $forTemplate;
        }
        
        public function actionShow($id) {           
            $item = $this->findInTableById("groups", $id);
            $id = $item->id;        

            $this->template->item = $this->prepareForTemplate($item);
            if($item['isFotoCollection']) {
                $fotoCollection = $this->database->table("foto_collections")->where("group_id = ?", $id)->limit(1)->fetch();
                $this->template->fotoCollection = $fotoCollection;
                $this->template->internalImages = $this->database->table("marionette_images")->where("code = ? AND type=?", $item->code, 1)->order("order ASC");
                $this->template->images = $this->database->table("marionette_images")->where("code = ? AND type!=?", $item->code, 1)->order("order ASC");
            }

            $this->template->marionettes = $this->databaseManager->getMarionettesFromGroup ($id);


        }

}
