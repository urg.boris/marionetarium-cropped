<?php

namespace App\Modules\Admin\Presenters;

use Nette;


class ManufacturerGroupPresenter extends BaseAdminPresenter {

	public function __construct(\App\Model\MarionetteManager $databaseManager, Nette\Database\Context $database, \App\Model\GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

	public function actionDefault() {

	}
        
        public function actionList() {
            $items = $this->database
                    ->table("groups")->where("main_section_id = ?", 2); //->limit(100);
            $this->template->items = [];
            foreach ($items as $item)
            {
                $this->template->items[] = $this->prepareForTemplate($item);
            }
        }

        private function prepareForTemplate($item): array {
            if(!$item->isFotoCollection) {
                throw new Exception("manufacturer group is not foto colection");
            }
            $forTemplate = [];
            $forTemplate["id"] = $item->id;
            $forTemplate["code"] = $item->code;
            $forTemplate['name'] =  $this->databaseManager->getLocaleByKey($item->locale_name);
            $forTemplate["age"] = $this->databaseManager->getLocaleByKey($item->locale_age);
            $forTemplate['cca'] = (int) $this->database
                        ->table("foto_collections")
                        ->where("group_id = ?", $item->id)
                        ->sum("orientationalMarionetteCount");
            return $forTemplate;
        }
        
        public function actionShow($id) {
            $item = $this->findInTableById("groups", $id);
            $id = $item->id;
            if(!$item->isFotoCollection) {
                throw new Exception("manufacturer group is not foto colection");
            }
            $this->template->item = $this->prepareForTemplate($item);

            $fotoCollection = $this->database->table("foto_collections")->where("group_id = ?", $id)->limit(1)->fetch();
            $this->template->fotoCollection = $fotoCollection;
            $this->template->internalImages = $this->database->table("marionette_images")->where("code = ? AND type=?", $item->code, 1)->order("order ASC");
            $this->template->images = $this->database->table("marionette_images")->where("code = ? AND type!=?", $item->code, 1)->order("order ASC");

        }

}
