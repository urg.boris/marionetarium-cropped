<?php

namespace App\Modules\Admin\Presenters;

use Nette;


class CarverPresenter extends BaseAdminPresenter {

	public function __construct(Nette\Database\Context $database, \App\Model\MarionetteManager $databaseManager, \App\Model\GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

	public function actionDefault() {

	}
        
        public function actionList() {
            $items = $this->database
                    ->table("carvers")->where("main_section_id = ?", 1); //->limit(100);
            $this->template->items = [];
            $m = null;
            foreach ($items as $item)
            {
                $this->template->items[] = $this->prepareForTemplate($item);
            }
        }

        private function prepareForTemplate($item): array {
            $forTemplate = [];
            $forTemplate["id"] = $item->id;
            $forTemplate["code"] = $item->code;
            $forTemplate["info"] = $item->info;
            $forTemplate['name'] = $this->databaseManager->getLocaleByKey($item->locale_name);
            $forTemplate['count'] = $this->database
                ->table("marionettes")
                ->where("carver_id = ?", $item->id)
                ->count("id");
            

            $forTemplate['cca'] = (int) $this->database
                    ->table("foto_collections")
                    ->where("carver_id = ?", $item->id)
                    ->sum("orientationalMarionetteCount");

            $forTemplate['totalCount'] = $forTemplate['count']+$forTemplate['cca'];
            return $forTemplate;
        }
        
        public function actionShow($id) {
            $item = $this->findInTableById("carvers", $id);
            $id = $item->id;   
            $this->template->item = $this->prepareForTemplate($item);
            $this->template->groups = $this->database->table("groups")->where("carver_id = ?", $id);
            $this->template->marionettes = $this->databaseManager->getMarionettesFromCarver($id);
       
        }

}
