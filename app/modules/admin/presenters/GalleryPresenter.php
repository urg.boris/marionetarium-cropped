<?php

namespace App\Modules\Admin\Presenters;

use App\Model\GalereyaGeneratorManager;
use Nette;

class GalleryPresenter extends BaseAdminPresenter {

        /**
         * @var GalereyaGeneratorManager
         */
        private $galereyaGeneratorManager;
    
	public function __construct(\App\Model\MarionetteManager $databaseManager, Nette\Database\Context $database, \App\Model\GuestKeyManager $guestKeyManager, \App\Model\GalereyaGeneratorManager $galereyaGeneratorManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
            $this->galereyaGeneratorManager = $galereyaGeneratorManager;
	}
        
        
	public function actionDefault($group_id, $carver_id) {
            $this->template->galleryDataFileName = $this->galereyaGeneratorManager->getGalleryDataFileName($group_id, $carver_id); //str_replace('\\', '/', DIRECTORY_SEPARATOR.$this->galereyaGeneratorManager->getCorespondingFileName($group_id, $carver_id));
            $this->template->galleryTitle = $this->galereyaGeneratorManager->getGaleryTitle($group_id, $carver_id);
            ////getGalleryDataFileName
            //var_dump($this->template->galleryDataFileName);
            //die;
	}

}
