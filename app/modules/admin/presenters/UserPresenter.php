<?php

namespace App\Modules\Admin\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Model\AuthorizatorFactory;
use App\Model\UserManager;

class UserPresenter extends BaseAdminPresenter {

	public function __construct(\App\Model\MarionetteManager $databaseManager, Nette\Database\Context $database, \App\Model\GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

    protected function createComponentSettingsForm() {

        $form = new Form;
        $username = $form->addText('username', 'Email sloužící jako přihlašovací jméno');
        if(!$this->getUser()->isInRole(AuthorizatorFactory::SUPERADMIN)) {
            $username->setDisabled(true);
        }   
        $name = $form->addText('name', 'Jméno')
                ->setRequired('Prosím vyplňte své jméno.');
        $roles = $form->addSelect('role', 'Role');
        if($this->getUser()->isInRole(AuthorizatorFactory::SUPERADMIN)) {
            $roles->setItems (AuthorizatorFactory::$roles);
        } else {
            $roles->setDisabled();
            $roles->setItems (AuthorizatorFactory::$roles);
        } 
        $form->addPassword('password1', 'Heslo');        
        $form->addPassword('password2', 'Potvrzení hesla');
        $form->addSubmit('submit', 'Uložit');        
        $id = $this->getParameter('id');
        if($id) {
            $user = $this->database->table("users")->get($id);        
            if(!$user) {
                throw new Nette\Application\BadRequestException;
            }            
            $username->setValue($user->username);
            $name->setValue($user->name);
            $roles->setValue($user->role);
        }   
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }
    
    public function formSucceeded(Form $form, Nette\Utils\ArrayHash $values)
    {
        $id = $this->getParameter('id');
        if ($id) {
            $this->checkUserSettingsAccessForId($id);

            $user = $this->database->table("users")->get($id);
            
            $dbValues = [
                "name" => $values->name,                 
                "update" => date("Y-m-d H:i:s"),
            ];
            
            if($values->password1 != "") {
                if($values->password1 != $values->password2) {
                    $this->flashMessage('Hesla se neschodují', 'error');
                    return false;
                }   else {
                    $dbValues["pass"] = Nette\Security\Passwords::hash($values->password1);
                }
            }
            
            if($this->getUser()->isInRole(AuthorizatorFactory::SUPERADMIN)) {
                $dbValues["username"] = $values->username;
                $dbValues["role"] = $values->role;
            }
            $user->update($dbValues);
            $currentIdentity = $this->getUser()->getIdentity();
            
            foreach ($values as $attribute => $value) {
              $currentIdentity->$attribute = $value;
            }
        } else {
            $this->checkUserSettingsAccessForId($id);
            try {
                if($values->password1 != $values->password2 || $values->password1=="") {
                    $this->flashMessage('Hesla se neschodují', 'error');
                    return false;
                }
                $userManager = new UserManager($this->database);
                $row = $userManager->add($values->username, $values->name, $values->password1, $values->role);
                $id = $row->id;
            } catch (\App\Model\DuplicateNameException $ex) {
                $this->flashMessage('Duplicitní jméno pro admina!');
                return;
            }
        }     
        $this->flashMessage('Uloženo. '.date("Y-m-d H:i:s"), 'success');
        $this->redirect('settings', $id);
    }

    public function actionSettings($id) {
        $this->checkUserSettingsAccessForId($id);
    }

    private function checkUserSettingsAccessForId($id) {
        if($this->getUser()->isInRole(AuthorizatorFactory::SUPERADMIN)) {
            return true;
        }
        
        if($id!=$this->getUser()->id) {
            throw new Nette\Application\ForbiddenRequestException;
        }
        
        $user = $this->database->table("users")->get($id);        
        if(!$user) {
            throw new Nette\Application\BadRequestException;
        }
        
        return true;
    }
    
}
