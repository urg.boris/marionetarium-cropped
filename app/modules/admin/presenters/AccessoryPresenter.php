<?php
declare(strict_types=1);

namespace App\Modules\Admin\Presenters;

use App\Model\GuestKeyManager;
use Nette;
use App\Model\MarionetteManager;


final class AccessoryPresenter extends BaseAdminPresenter {

	public function __construct(Nette\Database\Context $database, MarionetteManager $databaseManager, GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

	public function actionDefault() {
            $this->redirect(":spareParts");
	}
        
        public function actionCurtains() {
            $accessory = $this->database->table("accessory")->where("name = ?", "curtains")->limit(1)->fetch();
            $this->template->accessoryContent = $accessory->content;
        }
        
        public function actionDecorations() {
            $accessory = $this->database->table("accessory")->where("name = ?", "decorations")->limit(1)->fetch();
            $this->template->accessoryContent = $accessory->content;
        }
        
        public function actionSpareParts() {
            $accessory = $this->database->table("accessory")->where("name = ?", "spareParts")->limit(1)->fetch();
            $this->template->accessoryContent = $accessory->content;
        }
        
        public function actionLiterature() {
            $accessory = $this->database->table("accessory")->where("name = ?", "literature")->limit(1)->fetch();
            $this->template->accessoryContent = $accessory->content;
        }
/*
        private function prepareForTemplate($item): array {
            $forTemplate = [];
            $forTemplate["id"] = $item->id;
            $forTemplate["code"] = $item->code;
            $forTemplate['name'] =  $this->database
                ->table("locales")
                ->select("value")
                ->where("name = ?", $this->lang.".".$item->locale_name)
                ->fetch()
                ->value;
            $forTemplate["carverId"] = $item->carver_id;
            $forTemplate["carver"] = $this->database
                ->table("locales")
                ->select("value")
                ->where("name = ?", "cs.carver.".$item->carver->code)
                ->fetch()->value;
            $forTemplate['count'] = (int) $this->database
                ->table("marionettes")
                ->where("group_id = ?", $item->id)
                ->count("id");
            $forTemplate['isFotoCollection'] =  $item->isFotoCollection;
            if( $item->isFotoCollection) {
                $forTemplate['cca'] = (int) $this->database
                        ->table("foto_collections")
                        ->where("group_id = ?", $item->id)
                        ->sum("orientationalMarionetteCount");
            } else {
                $forTemplate['cca'] = 0;
            }
            $forTemplate['totalCount'] = $forTemplate['count']+$forTemplate['cca'];
            return $forTemplate;
        }
        
        public function actionShow($id) {
            $item = $this->database->table("groups")->get($id);
            $this->template->item = $this->prepareForTemplate($item);
            if($item['isFotoCollection']) {
                $fotoCollection = $this->database->table("foto_collections")->where("group_id = ?", $id)->limit(1)->fetch();
                $this->template->fotoCollection = $fotoCollection;
                $this->template->internalImages = $this->database->table("marionette_images")->where("code = ? AND type=?", $item->code, 1)->order("order ASC");
                $this->template->images = $this->database->table("marionette_images")->where("code = ? AND type!=?", $item->code, 1)->order("order ASC");
            }
            
            
            $this->template->marionettes = $this->marionetteManager->getMarionettesFromGroup ($id);

        }
*/
}
