<?php

namespace App\Modules\Admin\Presenters;

use Nette;

class StatsPresenter extends BaseAdminPresenter {

	public function __construct(\App\Model\MarionetteManager $databaseManager, Nette\Database\Context $database, \App\Model\GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

	public function actionDefault() {
            $this->template->stats = $this->database->table("stats")->order("time DESC")->limit(1)->fetch();
	}
        

}
