<?php

namespace App\Modules\Admin\Presenters;

use Nette;
use App\Model\GuestKeyManager;


class MarionettePresenter extends BaseAdminPresenter {

	public function __construct(\App\Model\MarionetteManager $databaseManager, Nette\Database\Context $database, GuestKeyManager $guestKeyManager) {
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}

	public function actionDefault() {

	}
        
        public function actionList() {
            $this->template->marionettes = $this->databaseManager->getAll();
        }
        
        public function getMarionettesFromGroup($id): array {
            $mationettes = $this->database
                    ->table("marionettes")->where("group_id = ?", $id);
            $marionettesInGroup = [];
            $m = null;
            foreach ($mationettes as $marionette)
            {
                $marionetteArray = $this->prepareMarionetteForTemplate($marionette);
                $marionettesInGroup[] = $marionetteArray;
            }
            return $marionettesInGroup;
        }

        public function actionShow($id) {
            $item = $this->findInTableById("marionettes", $id);
            $id = $item->id;
            $this->template->marionette = $this->databaseManager->prepareMarionetteForTemplate($item, true);
            $this->template->internalImage = $this->database->table("marionette_images")->where("code = ? AND type=?", $item->code, 1)->order("type ASC")->limit(1)->fetch();
            $this->template->images = $this->database->table("marionette_images")->where("code = ? AND type!=?", $item->code, 1)->order("type ASC");
        }
}
