<?php
declare(strict_types=1);

namespace App\Modules\Admin\Presenters;

use Nette;
use App\Model\AuthorizatorFactory;
use Nette\Utils\Arrays;
use App\Model\MarionetteManager;
use App\Model\GuestKeyManager;


/**
 * Base presenter for all application presenters.
 */
abstract class BaseAdminPresenter extends Nette\Application\UI\Presenter
{
    	/** @var Nette\Database\Context */
	protected $database;
        
        /** @var MarionetteManager */
        protected $databaseManager;
        
        protected $lang = "cs";
        
        /** @persistent */
        public $key;
    
        private $guestKeyManager;
        
	public function __construct(Nette\Database\Context $database, MarionetteManager $databaseManager, GuestKeyManager $guestKeyManager) {
            $this->database = $database;
            $this->databaseManager = $databaseManager;
            $this->guestKeyManager = $guestKeyManager;
            parent::__construct();
	}
        
        protected function startup() {
            parent::startup();
            //not have secret key, must be logged
            if($this->guestKeyManager==null || $this->key != $this->guestKeyManager->getKey()) {              
                if (!$this->user->isLoggedIn()) {
                    if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                        $this->flashMessage('Odhlášeno z důvodu neaktivity.');
                    }
                    $this->redirect('Sign:in', ['backlink' => $this->storeRequest()]);
                }

                if (!$this->getUser()->isAllowed(AuthorizatorFactory::RESOURCE_BACKEND, AuthorizatorFactory::OPERATION_VIEW)) {
                    throw new Nette\Application\ForbiddenRequestException;
                }
            }
            
            $identity = $this->getUser()->getIdentity();
            $this->template->name = $identity->name;
            $this->template->id = $this->getUser()->getId();
            $this->template->role = $identity->role;
            $this->template->email = $identity->username;
        }
        
        protected function findInTableById(string $table, $id) {
            $item = $this->database->table($table)->get($id);

            if(!$item) {
                $item = $this->database->table($table)->where("code = ?", $id)->limit(1)->fetch();
                if(!$item) {
                    throw new \Nette\Application\BadRequestException("Entita $id nebyla v tabulce $table nalezena.");
                }
            }
            return $item;
        }
        /*
        protected function getLocaleByKey(string $key) {
            $locale = $this->database
                ->table("locales")
                ->select("value")
                ->where("name = ?", $this->lang.".".$key)
                ->fetch();
            if(!$locale) {
                //for fallbacking to key
                //$value=$key; OR
                //$value = "";
            } else {
                $value = $locale->value;
            }
            return $value;
        }
         * 
         */
}
