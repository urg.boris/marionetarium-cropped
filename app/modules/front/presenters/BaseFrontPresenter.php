<?php

namespace App\Modules\Front\Presenters;

use Nette;
use App\Model\MarionetteManager;
use App\Model\AuthorizatorFactory;
use App\Model\GuestKeyManager;


/**
 * Base presenter for all application presenters.
 */
abstract class BaseFrontPresenter extends Nette\Application\UI\Presenter
{

    /** @var Nette\Database\Context */
	protected $database;
        
        /** @var MarionetteManager */
        protected $databaseManager;
        
        protected $lang = "cs";
        
        /** @persistent */
        public $key;
        
        protected $guestKeyManager;
    
	public function __construct(Nette\Database\Context $database, MarionetteManager $databaseManager, GuestKeyManager $guestKeyManager) {
            parent::__construct();
            $this->database = $database;
            $this->databaseManager = $databaseManager;               
            $this->guestKeyManager = $guestKeyManager;
    }
        
        protected function startup() {
            parent::startup();
            //not have secret key, must be logged
            if($this->key != $this->guestKeyManager->getKey()) {              
                if (!$this->user->isLoggedIn()) {
                    if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                        $this->flashMessage('Odhlášeno z důvodu neaktivity.');
                    }
                    $this->redirect(':Admin:Sign:in', ['backlink' => $this->storeRequest()]);
                }

                if (!$this->getUser()->isAllowed(AuthorizatorFactory::RESOURCE_BACKEND, AuthorizatorFactory::OPERATION_VIEW)) {
                    throw new Nette\Application\ForbiddenRequestException;
                }
            }
            
        }
}
