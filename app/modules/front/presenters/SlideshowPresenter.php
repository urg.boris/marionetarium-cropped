<?php

namespace App\Modules\Front\Presenters;

use App\Model\GuestKeyManager;
use App\Model\MarionetteManager;
use Nette;
use Nette\Utils\FileSystem;

class SlideshowPresenter extends BaseFrontPresenter {

        private $output = "";
        private $relativeFileName = DIRECTORY_SEPARATOR."js".DIRECTORY_SEPARATOR."slideshow.json";
        private $absolutFileName;
        private $wwwDir;
	public function __construct(string $wwwDir, MarionetteManager $databaseManager, Nette\Database\Context $database, GuestKeyManager $guestKeyManager) {
            $this->wwwDir = $wwwDir;
            $this->absolutFileName = $this->wwwDir.$this->relativeFileName;
            parent::__construct($database, $databaseManager, $guestKeyManager);
	}
        
        public function actionDefault() {
            $this->template->slideshowData = $this->getData();
        }
        
        public function generateData() {
            $items = $this->database
                    ->table("marionette_images")->where("type", [2,3])->order('code ASC');
            $name = "";
            $code = "";
            $this->output = "";
            $itemsCount = count($items);
            $i=0;
            $link = "";
            $mar = null;
            foreach ($items as $item) {
                if($item->type == 2 || $item->type == 3) {
                    $code = $item->code;
                    $name = $this->databaseManager->getLocaleByKey('marionette.'.$item->code);
                    $mar = $this->database->table("marionettes")->where("code",$item->code)->limit(1)->fetch();
                    $link = " <a href=\"/admin/marionette/show/".$mar->id."?key=".$this->guestKeyManager->getKey()."\">(přechod na detail loutky)</a>";
                    
                } else {
                    $code = $item->code;
                    $name = "";
                    $link = "";
                }
                $pathString = "/data/".$item->path; 
                $this->output .= sprintf("{'src': '%s', 'thumb': '%s', 'subHtml': '<h4>%s</h4><p>%s%s</p>'}", $pathString, $pathString, $code, $name, $link);
                if ($i != $itemsCount - 1) {
                    $this->output .= ",\n";
                }
                $i++;
            }
            FileSystem::write($this->absolutFileName, $this->output, 0755);
	}
        
        private function getData() {
            if(file_exists($this->absolutFileName)) {
                return file_get_contents($this->absolutFileName);
            } else {
                $this->generateData();
                //was it generated?
                if(file_exists($this->absolutFileName)) {
                    return $this->output;
                } else {
                    return null;
                }
            }
            
        }

}
