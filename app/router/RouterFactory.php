<?php

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
                $router[] = new Route('', [
                    'module' => 'Admin',
                    'presenter' => 'Homepage',
                    'action' => 'default',
                ]);
               $router[] = new Route('<module>/<presenter>/<action>[/<id>]', [
                    'module' => 'Admin',
                    'presenter' => 'Homepage',
                    'action' => 'default',
                ]);
		return $router;
	}

}
