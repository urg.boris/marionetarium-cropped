<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require __DIR__ . '/.maintenance.php';

// initialize the environment + get DI container
$container = require __DIR__ . '/../app/bootstrap.php';
// DI container creates a Nette\Application\Application object
$application = $container->getByType(Nette\Application\Application::class);
// start Nette application
$application->run();

